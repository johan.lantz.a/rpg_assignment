﻿using AssignmentOne.Items;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AssignmentOne.Character
{
    public class Mage : PlayerClass
    {
        public Mage()
        {
            
            strengthGain = 1;
            strength = (level - 1) * strengthGain + 1;
            
            dexterityGain = 1;
            dexterity = (level - 1) * dexterityGain + 1;

            intelligenceGain = 5;
            intelligence = (level - 1) * intelligenceGain + 8;
            

            primaryDamageStat = intelligence;
            primaryDamageStatGain = intelligenceGain;

            vitalityGain = 3;
            vitality = (level - 1) * vitalityGain + 14;

            equipableArmors = new ArmorTypes[] {ArmorTypes.CLOTH };
            equipableWeapons = new WeaponTypes[] {WeaponTypes.WAND, WeaponTypes.STAFF };
            
        }
    }
}
