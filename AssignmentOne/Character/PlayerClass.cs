﻿using AssignmentOne.CustomExceptions;
using AssignmentOne.Items;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AssignmentOne.Character
{
     public class PlayerClass
    {   
        public PlayerClass()
        {
            equipment = new Dictionary<Slots, object>();

        }
    
        public Dictionary<Slots, object> equipment { get; set; }
        public int strength { get; set; }
        public int dexterity { get; set; }
        public int intelligence { get; set; }
        public int strengthGain { get; set; }
        public int dexterityGain { get; set; }
        public int intelligenceGain { get; set; }
        public int level { set; get; } = 1;
        public double primaryDamageStat { set; get; }
        public int primaryDamageStatGain { set; get; }

        public int vitality  { set; get; }
        public int vitalityGain { set; get; }

        public double experience { set; get; } = 0;



        public WeaponTypes[] EquipableWeapons { get => equipableWeapons; }

        private protected WeaponTypes[] equipableWeapons;

        public ArmorTypes[] EquipableArmor { get => equipableArmors; }

        private protected ArmorTypes[] equipableArmors;

   

        public string Name { set; get; }  = string.Empty;

        public double characterDPS { get; set; }   

        /// <summary>
        /// Adds 1 to level each time this function is called. adds the gain version of every attribute when the function is called. 
        /// </summary>
        public void levelUp()
        {
            level++;
            strength += strengthGain;
            dexterity += dexterityGain;
            intelligence += intelligenceGain;
            primaryDamageStat += primaryDamageStatGain;
            vitality += vitalityGain;
        }

        /// <summary>
        /// checks if a weaponitem is present in the equipment dictionary. If not, the damage of the character is set 1 with the increase from the primary attribute
        /// (intelligence for mage...). If a weapon is present in the dictionary, the dps is equipped weapons dps multiplied with the characters damage.
        /// </summary>

        public void calcDamage()
        {
            if (equipment.ContainsKey(Slots.WEAPON))
            {
                Weapon equipedWep = (Weapon)equipment[Slots.WEAPON];
                double statBonus = 1 + (primaryDamageStat / 100.0);
                characterDPS = equipedWep.DPS * statBonus;
                characterDPS = Math.Round(characterDPS, 4);

            }
            else
            {
                double statBonus = 1 + (primaryDamageStat / 100.0);
                characterDPS = 1 * statBonus;
                characterDPS = Math.Round(characterDPS, 4);
            }
        }

        /// <summary>
        /// First this function uses an if statement to determine if there is an item already in the slot where the specified armor item fits. If there is an item there,
        /// the stats of the already present item is removed from the characters total stats. In this current version of this asssignment the items only roll with 
        /// stats appropiate to the armortypes, meaning an cloth item rolls with intelligence, plate with strength, leather with dexterity, and mail with a strength
        /// and dexterity blend. After the already present items stats are removed, the attributes of the new armor item is added to primaryDamageStat (which is just
        ///  a univeral name for stat the the characterclasses scale their damage from)
        /// </summary>
        /// <param name="playerClass"></param>
        /// <param name="armor"></param>
        public void calcTotalMainStat( Armor armor)
        {
            if (equipment.ContainsKey(armor.slot))
            {
                Armor temp = (Armor)equipment[armor.slot];
                if (temp.armorType == ArmorTypes.CLOTH)
                {
                    primaryDamageStat -= temp.bonusIntelligence;
                }
                else if (temp.armorType == ArmorTypes.LEATHER)
                {
                    primaryDamageStat -= temp.bonusDexterity;
                }
                else if (temp.armorType == ArmorTypes.MAIL && dexterity > strength)
                {
                    primaryDamageStat -= temp.bonusDexterity;
                }
                else if (temp.armorType == ArmorTypes.MAIL && dexterity < strength)
                {
                    primaryDamageStat -= temp.bonusStrength;
                }
                else if (temp.armorType == ArmorTypes.PLATE)
                {
                    primaryDamageStat -= temp.bonusStrength;
                }
            }
            
            if (armor.armorType == ArmorTypes.CLOTH)
            {
                primaryDamageStat += armor.bonusIntelligence;
            }
            else if (armor.armorType == ArmorTypes.LEATHER)
            {
                primaryDamageStat += armor.bonusDexterity;
            }
            else if (armor.armorType == ArmorTypes.MAIL && dexterity > strength)
            {
                primaryDamageStat += armor.bonusDexterity;
            }
            else if (armor.armorType == ArmorTypes.MAIL && dexterity < strength)
            {
                primaryDamageStat += armor.bonusStrength;
            }
            else if (armor.armorType == ArmorTypes.PLATE)
            {
                primaryDamageStat += armor.bonusStrength;
            }
            
        }



        /// <summary>
        /// Function that shows specified characters name, level, and attributes. It calls upon the calcDamage to update the characters damage before returnin stats
        /// and showing it in the console via the program.cs file.
        /// </summary>
        /// <param name="playerClass"></param>
        /// <returns>return a string with information related to character (stats)</returns>
        public static string showStats(PlayerClass playerClass)
        {
            string stats = $"Your character name is: {playerClass.Name}. You are level: {playerClass.level}. " +
                $"Your stats are... Strength: {playerClass.strength}. Dexterity: {playerClass.dexterity}. Intelligence: {playerClass.intelligence}";
            playerClass.calcDamage();
            return stats;
        }

        /// <summary>
        /// The function takes the character element and weapon elements and first checks if the required level is higher than character level and throws an error if 
        /// it is. If the characters level is equal or higher than the required level, and if statement checks if the weapontype is contained in the characters'
        /// equipableWeapons (which is an array with weapontypes allowed for that character class). If the weapontype is allowed the weapon is added to equipment dictionary
        /// at the weaponslot. the calcDamage function is called upon to update the characters damage, and a message is returned. An exceptions is thrown if the 
        /// weapontype is not in the equipableWeapons array.
        /// </summary>
        /// <param name="playerClass"></param>
        /// <param name="newWep"></param>
        /// <returns>Retuns a message of "new weapon equipped"</returns>
        /// <exception cref="WeaponException"></exception>

        public string equipWep(PlayerClass playerClass ,Weapon newWep)
        {

            if (playerClass.level < newWep.requiredLevel)
            {
                throw new WeaponException("Too low level to equip this weapon");
            }
            else
            {
            
            if ( equipableWeapons.Contains(newWep.weapontype))
            {
                equipment[Slots.WEAPON] = newWep;
                playerClass.calcDamage();
                    return "New weapon equipped";
            }
            else
            {
                throw new WeaponException("Cant equip that weapontype");
            }
            }



        }


        /// <summary>
        /// This function checks if the character level is equals too or above the required of the armor item. Then if / else if statements checks which slot the 
        /// new armor item fits into. After identifying which slot, another if statements checks if the armortype fits within the arrays of equipableArmors that 
        /// checks for armortype. If the armor fits into these statements its stats are added to the character with the calcTotalMainStat function and an update to 
        /// the characters damage is done with the calcDamage function. After these functions the armor item is added to the equipment dictionary and a string is returned.
        /// If at any moment the if / else if statements determines that the armor is not allowed for the current character an ArmorExceptions is thrown.
        /// </summary>
        /// <param name="playerClass"></param>
        /// <param name="newArmor"></param>
        /// <returns>Either message that the item is equipped, or throws and error</returns>
        /// <exception cref="ArmorException"></exception>
        public string equipArmor(PlayerClass playerClass, Armor newArmor)
        {

            if (playerClass.level >= newArmor.requiredLevel)
            {
                if (newArmor.slot == Slots.HEAD)
                {
                    if (playerClass.equipableArmors.Contains<ArmorTypes>(newArmor.armorType))
                    {
                        calcTotalMainStat(newArmor);
                        calcDamage();
                        playerClass.equipment[Slots.HEAD] = newArmor;
                        return "New armor equipped!";

                    }
                    else
                    {
                        throw new ArmorException("Cant equip that armortype!");

                    }
                }
                else if (newArmor.slot == Slots.BODY)
                {
                    if (playerClass.equipableArmors.Contains<ArmorTypes>(newArmor.armorType))
                    {
                        calcTotalMainStat(newArmor);
                        calcDamage();
                        playerClass.equipment[Slots.BODY] = newArmor;
                        return "New armor equipped!";

                    }
                    else
                    {
                        throw new ArmorException("Cant equip that armortype!");

                    }
                }
                else if (newArmor.slot == Slots.LEGS)
                {

                    if (playerClass.equipableArmors.Contains<ArmorTypes>(newArmor.armorType))
                    {
                        calcTotalMainStat(newArmor);
                        calcDamage();
                        playerClass.equipment[Slots.LEGS] = newArmor;
                        return "New armor equipped!";

                    }
                    else
                    {
                        throw new ArmorException("Cant equip that armortype!");

                    }

                }
                else
                { // this return is so i dont have to restructure function. Otherwise i get "not all paths returns value"
                    return "Something went wrong :( ";
                }
            }
            else
            {

                throw new ArmorException("Too low level too equip this piece of armor");
            }
        }  





        /// <summary>
        /// The fightMonster function takes the playercharacter and a monster generated before this functioncall and starts the battle between them. For every
        /// action the user takes the monster will make one action afterwards. First the monsters damage is defined after calling on the monsterAction function
        /// which determines if the monster will do normal attack, crit attack or defend against players attack. If the monster does not defend, the player
        /// dps is directly affecting monster health, and the monsters normal or crit attack is directly affecting the player temphealth which is declared
        /// in the begining of the function to be equal to player.vitality. If monsterhealth goes lower than 0 the monsters expYield is applied to player
        /// and a random loot (1 or 2 roll for weapon or armor) is generated via CreateWeaponLoot or CreateArmorLoot functioncalls.
        /// 
        /// If the input is 2 for counter, the monster damage is reduced and a portion of the monster attack is applied to damage monster health. 
        /// The damage to monster health from counter is 30% of monsters applied damage to player in case of normal hit, and 150% of damage applied to player
        /// in case of a critical hit from the monster. Once again if monster dies from counter, loot is generated.
        /// 
        /// If the players health reaches 0 or bellow a message is written and the console application shuts down.
        /// 
        /// In case of input 3 to flee, the user is taken back to basic menu of fightning monster, showing stats or cheating for new equipment.
        /// </summary>
        /// <param name="player"></param>
        /// <param name="monster"></param>

        public void fightMonster(PlayerClass player, Monster monster)
        {
            double tempHealth = player.vitality;
            bool action = true;

           

            double tempMonsterHealth = monster.monsterHealth;

            do
            {
                Console.WriteLine($"You have {tempHealth} health and {player.characterDPS} dps");
                Console.WriteLine($"You are facing a level {monster.monsterLevel} {monster.monsterName}. It has {monster.monsterDamage} damage and {tempMonsterHealth} health");
                Console.WriteLine("Enter input to take action!");
                Console.WriteLine("Enter 1 to attack, 2 to counter, 3 to flee");
                bool counter = false;
                double monsterDmg = 0;
                string input = Console.ReadLine();
                Random random = new Random();
                int potentialLoot = random.Next(1,3);
               

                if (input == "1")
                {
       
                   monsterDmg =  monster.monsterAction(monster, counter);
                    if (monster.monsterDefence == false)
                    {
                        tempMonsterHealth -= player.characterDPS;
                        tempHealth -= monsterDmg;
                        if (tempMonsterHealth <= 0 )
                        {
                            Console.WriteLine("Monster defeated!");
                            player.experience += monster.expYield;
                            Console.WriteLine($"you gained {monster.expYield} experience!");
                            do
                            { 
                                if (player.experience >= 10)
                                {
                                    player.experience = monster.expYield - 10;
                                    player.levelUp();
                                }
                            } while (player.experience > 10);

                            do
                            {


                                if (potentialLoot == 1)
                                {
                                    var newWeapon = Weapon.CreateWeaponLoot(player);
                                    Console.WriteLine($"The monster dropped a {newWeapon.itemName} for level {newWeapon.requiredLevel} with {newWeapon.DPS} dps");
                                    Console.WriteLine("Do you want to equip it? Enter 1 to equip, or other input to exit");
                                    string equip = Console.ReadLine();
                                    if (equip == "1")
                                    {
                                        player.equipWep(player, newWeapon);
                                        break;
                                    }
                                    else
                                    {
                                        break;
                                    }
                                }
                                else if (potentialLoot == 2)
                                {
                                    var newArmor = Armor.CreateArmorLoot(player);
                                    Console.WriteLine($"The monster dropped a {newArmor.itemName}");
                                    Console.WriteLine("Do you want to equip it? Enter 1 to equip, or other input to exit");
                                    string equip = Console.ReadLine();
                                    if (equip == "1")
                                    {
                                        player.equipArmor(player, newArmor);
                                        break;
                                    }
                                    else
                                    {
                                        break;
                                    }
                                }
                            } while (true);

                            break;
                        }
                        else if (tempHealth <= 0)
                        {
                            Console.WriteLine("You died!");
                            Environment.Exit(0);
                        }
                    }
                    else if (monster.monsterDefence == true)
                    {
                        Console.WriteLine("The monster defended against half your attack!");
                        tempMonsterHealth -= player.characterDPS / 2;
                        monster.monsterDefence = false;

                        
                        if (tempMonsterHealth <= 0)
                        {
                            Console.WriteLine("Monster defeated!");
                            player.experience += monster.expYield;
                            do
                            {
                                if (player.experience >= 10)
                                {
                                    player.experience = monster.expYield - 10;
                                    player.levelUp();
                                }
                            } while (player.experience > 10);
                            do
                            {


                                if (potentialLoot == 1)
                                {
                                    var newWeapon = Weapon.CreateWeaponLoot(player);
                                    Console.WriteLine($"The monster dropped a {newWeapon.itemName} for level {newWeapon.requiredLevel} with {newWeapon.DPS} dps");
                                    Console.WriteLine("Do you want to equip it? Enter 1 to equip, or other input to exit");
                                    string equip = Console.ReadLine();
                                    if (equip == "1")
                                    {
                                        player.equipWep(player, newWeapon);
                                        break;
                                    }
                                    else
                                    {
                                        break;
                                    }
                                }
                                else if (potentialLoot == 2)
                                {
                                    var newArmor = Armor.CreateArmorLoot(player);
                                    Console.WriteLine($"The monster dropped a {newArmor.itemName}");
                                    Console.WriteLine("Do you want to equip it? Enter 1 to equip, or other input to exit");
                                    string equip = Console.ReadLine();
                                    if (equip == "1")
                                    {
                                        player.equipArmor(player, newArmor);
                                        break;
                                    }
                                    else
                                    {
                                        break;
                                    }
                                }
                            } while (true);

                            break;
                        }
                        else if (tempHealth <= 0)
                        {
                            Console.WriteLine("You died!");
                            Environment.Exit(0);
                        }
                    }
                }
                else if (input == "2")
                {
                    counter = true;
                    monsterDmg = monster.monsterAction(monster, counter);
                    tempHealth -= monster.monsterDamage;
                    if (monsterDmg == monster.monsterDamage)
                    {
                        Console.WriteLine("You countered the monsters critical attack and returned 150% of his damage!");
                        tempMonsterHealth -= monsterDmg * 1.5;
                    }
                    else
                    {
                        Console.WriteLine("you countered a normal attack from the monster and returned 30% of his damage");
                        tempMonsterHealth -= monsterDmg * 0.3;
                    }


                    if (tempMonsterHealth <= 0)
                    {
                        Console.WriteLine("Monster defeated!");
                        player.experience += monster.expYield;
                        do
                        {
                            if (player.experience >= 10)
                            {
                                player.experience = monster.expYield - 10;
                                player.levelUp();
                            }
                        } while (player.experience > 10);
                        do
                        {

                        
                        if (potentialLoot == 1)
                        {
                            var newWeapon = Weapon.CreateWeaponLoot(player);
                            Console.WriteLine($"The monster dropped a {newWeapon.itemName} for level {newWeapon.requiredLevel} with {newWeapon.DPS} dps");
                            Console.WriteLine("Do you want to equip it? Enter 1 to equip, or other input to exit");
                            string equip = Console.ReadLine();
                            if (equip == "1")
                            {
                                player.equipWep(player, newWeapon);
                                    break;
                            }
                            else
                            {
                                break;
                            }
                            }
                        else if (potentialLoot == 2)
                        {
                            var newArmor = Armor.CreateArmorLoot(player);
                            Console.WriteLine($"The monster dropped a {newArmor.itemName}");
                            Console.WriteLine("Do you want to equip it? Enter 1 to equip, or other input to exit");
                            string equip = Console.ReadLine();
                            if (equip == "1")
                            {
                                player.equipArmor(player, newArmor);
                                break;
                            }
                            else
                            {
                                break;
                            }
                            }
                        } while (true);

                        break;
                    }
                    else if (tempHealth <= 0)
                    {
                        Console.WriteLine("You died!");
                        Environment.Exit(0);
                    }
                }
                else if (input == "3")
                {
                    break;
                }
                else
                {
                    Console.WriteLine("Wrong input");
                }

            } while (action == true);

        }

    }
}

