﻿using AssignmentOne.Items;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AssignmentOne.Character
{
    public class Ranger : PlayerClass
    {
        public Ranger()
        {
            strengthGain = 1;
            strength = (level - 1) * strengthGain + 1;

            dexterityGain = 5;
            dexterity = (level - 1) * dexterityGain + 7;

            intelligenceGain = 1;
            intelligence = (level - 1) * intelligenceGain + 1;

            primaryDamageStat = dexterity;
            primaryDamageStatGain = dexterityGain;

            vitalityGain = 3;
            vitality = (level - 1) * vitalityGain + 20;

            equipableArmors = new ArmorTypes[] { ArmorTypes.LEATHER, ArmorTypes.MAIL };
            equipableWeapons = new WeaponTypes[] { WeaponTypes.BOW };

        }
    }
}
