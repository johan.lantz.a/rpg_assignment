﻿using AssignmentOne.Items;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AssignmentOne.Character
{
    public class Rogue : PlayerClass
    {
        public Rogue()
        {
            strengthGain = 1;
            strength = (level - 1) * strengthGain + 2;

            dexterityGain = 4;
            dexterity = (level - 1) * dexterityGain + 6;

            intelligenceGain = 1;
            intelligence = (level - 1) * intelligenceGain + 1;

            primaryDamageStat = dexterity;
            primaryDamageStatGain = dexterityGain;

            vitalityGain = 4;
            vitality = (level - 1) * vitalityGain + 20;

            equipableArmors = new ArmorTypes[] { ArmorTypes.LEATHER, ArmorTypes.MAIL };
            equipableWeapons = new WeaponTypes[] { WeaponTypes.DAGGER, WeaponTypes.SWORD };

        }
    }
}
