﻿
using AssignmentOne.Items;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AssignmentOne.Character
{
    public class Warrior : PlayerClass
    {
        public Warrior()
        {
            strengthGain = 3;
            strength = (level - 1) * strengthGain + 5;

            dexterityGain = 2;
            dexterity = (level - 1) * dexterityGain + 2;

            intelligenceGain = 1;
            intelligence = (level - 1) * intelligenceGain + 1;

            vitalityGain = 6;
            vitality = (level - 1) * vitalityGain + 30;

            primaryDamageStat = strength;
            primaryDamageStatGain = strengthGain;

            equipableArmors = new ArmorTypes[] {  ArmorTypes.MAIL, ArmorTypes.PLATE };
            equipableWeapons = new WeaponTypes[] { WeaponTypes.SWORD, WeaponTypes.HAMMER, WeaponTypes.AXE };

        }

    

    }
}
