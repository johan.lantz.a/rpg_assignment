﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AssignmentOne.CustomExceptions
{
    public class ArmorException : Exception
    {
        public ArmorException(string? message) : base(message)
        {
        }
    }
}
