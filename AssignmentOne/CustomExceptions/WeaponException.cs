﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AssignmentOne.CustomExceptions
{
    public class WeaponException : Exception
    {
        public WeaponException(string? message) : base(message)
        {
        }
    }
}
