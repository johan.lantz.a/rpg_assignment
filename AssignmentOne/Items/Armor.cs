﻿using AssignmentOne.Character;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AssignmentOne.Items
{
    public class Armor : GenericItem
    {
        public int bonusStrength { get; set; }
        public int bonusDexterity { get; set; }
        public int bonusIntelligence { get; set; }

        public ArmorTypes armorType  { get; set; }


        /// <summary>
        /// Function to use random functions to generate a new Armor item. First if else determines the slot of the armor, the second if else determines the armortype,
        /// and the third if else (the long one) determines the stats which also determine the level requirement and name of the armor piece.
        /// </summary>
        /// <returns> Armor item named armor </returns>
        public static Armor CreateArmor()
        {
            Armor armor = new Armor();
            Random random = new Random();

            int armorPiece = random.Next(1,4);

            // Assigning slot to the new armor item depending on armorPiece roll

            if (armorPiece == 1)
            {
                armor.slot = Slots.HEAD;
            }
            else if (armorPiece == 2)
            {
                armor.slot = Slots.BODY;
            }
            else if (armorPiece == 3)
            {
                armor.slot = Slots.LEGS;
            }
 

            int typeRoll = random.Next(1, 5);

            // Assigning ArmorType to the new armor item depending on typeRoll

            if (typeRoll == 1)
            {
                armor.armorType = ArmorTypes.PLATE;
            }
            else if (typeRoll == 2)
            {
                armor.armorType = ArmorTypes.MAIL;
            }
            else if (typeRoll == 3)
            {
                armor.armorType = ArmorTypes.LEATHER;
            }
            else if (typeRoll == 4)
            {
                armor.armorType = ArmorTypes.CLOTH;
            }



            int statRoll = random.Next(1, 21);


            // stat roll for cloth gear
            if (statRoll <= 5 && armor.armorType == ArmorTypes.CLOTH)
            {
                armor.bonusIntelligence = statRoll;
                armor.itemName = $"Poor {armor.armorType} {armor.slot}";
                armor.requiredLevel = 1;
            }
            else if (statRoll >5 && statRoll <= 10 && armor.armorType == ArmorTypes.CLOTH)
            {
                armor.bonusIntelligence = statRoll;
                armor.itemName = $"Normal {armor.armorType} {armor.slot}";
                armor.requiredLevel = 3;
            }
            else if (statRoll > 10 && statRoll <= 15 && armor.armorType == ArmorTypes.CLOTH)
            {
                armor.bonusIntelligence = statRoll;
                armor.itemName = $"Greater {armor.armorType} {armor.slot}";
                armor.requiredLevel = 5;
            }
            else if (statRoll > 15 && statRoll <= 20 && armor.armorType == ArmorTypes.CLOTH)
            {
                armor.bonusIntelligence = statRoll;
                armor.itemName = $"Tal Rasha's {armor.armorType} {armor.slot}";
                armor.requiredLevel = 7;
            }
           
            // stat roll for leather gear
            else if (statRoll <= 5 && armor.armorType == ArmorTypes.LEATHER)
            {
                armor.bonusDexterity = statRoll;
                armor.itemName = $"Poor {armor.armorType} {armor.slot}";
                armor.requiredLevel = 1;
            }
            else if (statRoll > 5 && statRoll <= 10 && armor.armorType == ArmorTypes.LEATHER)
            {
                armor.bonusDexterity = statRoll;
                armor.itemName = $"Normal {armor.armorType} {armor.slot}";
                armor.requiredLevel = 3;
            }
            else if (statRoll > 10 && statRoll <= 15 && armor.armorType == ArmorTypes.LEATHER)
            {
                armor.bonusDexterity = statRoll;
                armor.itemName = $"Greater {armor.armorType} {armor.slot}";
                armor.requiredLevel = 5;
            }
            else if (statRoll > 15 && statRoll <= 20 && armor.armorType == ArmorTypes.LEATHER)
            {
                armor.bonusDexterity = statRoll;
                armor.itemName = $"Vidala's {armor.armorType} {armor.slot}";
                armor.requiredLevel = 7;
            }

            // Stat roll for mail gear
            else if (statRoll <= 5 && armor.armorType == ArmorTypes.MAIL)
            {
                if ( statRoll % 2 != 0) { statRoll += 1; }
                armor.bonusDexterity = statRoll / 2;
                armor.bonusStrength = statRoll / 2;
                armor.itemName = $"Poor {armor.armorType} {armor.slot}";
                armor.requiredLevel = 1;
            }
            else if (statRoll > 5 && statRoll <= 10 && armor.armorType == ArmorTypes.MAIL)
            {
                if (statRoll % 2 != 0) { statRoll += 1; }
                armor.bonusDexterity = statRoll / 2;
                armor.bonusStrength = statRoll / 2;
                armor.itemName = $"Normal {armor.armorType} {armor.slot}";
                armor.requiredLevel = 3;
            }
            else if (statRoll > 10 && statRoll <= 15 && armor.armorType == ArmorTypes.MAIL)
            {
                if (statRoll % 2 != 0) { statRoll += 1; }
                armor.bonusDexterity = statRoll / 2;
                armor.bonusStrength = statRoll / 2;
                armor.itemName = $"Greater {armor.armorType} {armor.slot}";
                armor.requiredLevel = 5;
            }
            else if (statRoll > 15 && statRoll <= 20 && armor.armorType == ArmorTypes.MAIL)
            {
                if (statRoll % 2 != 0) { statRoll += 1; }
                armor.bonusDexterity = statRoll / 2;
                armor.bonusStrength = statRoll / 2;
                armor.itemName = $"Milabrega's {armor.armorType} {armor.slot}";
                armor.requiredLevel = 7;
            }

            //stat roll for plate gear
            else if (statRoll <= 5 && armor.armorType == ArmorTypes.PLATE)
            {
                armor.bonusStrength = statRoll;
                armor.itemName = $"Poor {armor.armorType} {armor.slot}";
                armor.requiredLevel = 1;
            }
            else if (statRoll > 5 && statRoll <= 10 && armor.armorType == ArmorTypes.PLATE)
            {
                armor.bonusStrength = statRoll;
                armor.itemName = $"Normal {armor.armorType} {armor.slot}";
                armor.requiredLevel = 3;
            }
            else if (statRoll > 10 && statRoll <= 15 && armor.armorType == ArmorTypes.PLATE)
            {
                armor.bonusStrength = statRoll;
                armor.itemName = $"Greater {armor.armorType} {armor.slot}";
                armor.requiredLevel = 5;
            }
            else if (statRoll > 15 && statRoll <= 20 && armor.armorType == ArmorTypes.PLATE)
            {
                armor.bonusStrength = statRoll;
                armor.itemName = $"Immortal king's {armor.armorType} {armor.slot}";
                armor.requiredLevel = 7;
            }
            return armor;
        }


        /// <summary>
        /// Similar function to earlier CreateArmor function, but with implementations to limit the item to one that the player object can equip. 
        /// This function is only called once a monster dies in the fightMonster function (inside of playerClass file)
        /// </summary>
        /// <param name="player"></param>
        /// <returns></returns>
        public static Armor CreateArmorLoot(PlayerClass player)
        {
            Armor armor = new Armor();
            Random random = new Random();

            int armorPiece = random.Next(1, 4);

            // Assigntning Slot to the newly created armor item

            if (armorPiece == 1)
            {
                armor.slot = Slots.HEAD;
            }
            else if (armorPiece == 2)
            {
                armor.slot = Slots.BODY;
            }
            else if (armorPiece == 3)
            {
                armor.slot = Slots.LEGS;
            }


            int typeRoll = 0;


            // limiting the ArmorTypes for the newly created item to only be one that the player object can equip.

            if (player.EquipableArmor.Contains(ArmorTypes.PLATE) && player.EquipableArmor.Contains(ArmorTypes.MAIL))
            {
                typeRoll = random.Next(1,3);
            }
            else if (player.EquipableArmor.Contains(ArmorTypes.MAIL) && player.EquipableArmor.Contains(ArmorTypes.LEATHER))
            {
                typeRoll = random.Next(2, 4);
            }
            else if (player.EquipableArmor.Contains(ArmorTypes.CLOTH))
            {
                typeRoll = 4;
            }

            // Assigning the ArmorType to the newly created armor item

            if (typeRoll == 1)
            {
                armor.armorType = ArmorTypes.PLATE;
            }
            else if (typeRoll == 2)
            {
                armor.armorType = ArmorTypes.MAIL;
            }
            else if (typeRoll == 3)
            {
                armor.armorType = ArmorTypes.LEATHER;
            }
            else if (typeRoll == 4)
            {
                armor.armorType = ArmorTypes.CLOTH;
            }



            int statRoll = random.Next(1, 21);

            // Limiting the statRoll for the armor to only assign stats and levelrequirements that the player object can equip.

            if (player.level < 3 && statRoll > 5)
            {
                statRoll = 5;
            }
            else if (player.level >= 3 && player.level < 5 && statRoll > 10)
            {
                statRoll = 10;
            }
            else if (player.level >= 5 && player.level < 7 && statRoll > 15)
            {
                statRoll = 15;
            }
 
            // stat roll for cloth gear
            if (statRoll <= 5 && armor.armorType == ArmorTypes.CLOTH)
            {
                armor.bonusIntelligence = statRoll;
                armor.itemName = $"Poor {armor.armorType} {armor.slot}";
                armor.requiredLevel = 1;
            }
            else if (statRoll > 5 && statRoll <= 10 && armor.armorType == ArmorTypes.CLOTH)
            {
                armor.bonusIntelligence = statRoll;
                armor.itemName = $"Normal {armor.armorType} {armor.slot}";
                armor.requiredLevel = 3;
            }
            else if (statRoll > 10 && statRoll <= 15 && armor.armorType == ArmorTypes.CLOTH)
            {
                armor.bonusIntelligence = statRoll;
                armor.itemName = $"Greater {armor.armorType} {armor.slot}";
                armor.requiredLevel = 5;
            }
            else if (statRoll > 15 && statRoll <= 20 && armor.armorType == ArmorTypes.CLOTH)
            {
                armor.bonusIntelligence = statRoll;
                armor.itemName = $"Tal Rasha's {armor.armorType} {armor.slot}";
                armor.requiredLevel = 7;
            }

            // stat roll for leather gear
            else if (statRoll <= 5 && armor.armorType == ArmorTypes.LEATHER)
            {
                armor.bonusDexterity = statRoll;
                armor.itemName = $"Poor {armor.armorType} {armor.slot}";
                armor.requiredLevel = 1;
            }
            else if (statRoll > 5 && statRoll <= 10 && armor.armorType == ArmorTypes.LEATHER)
            {
                armor.bonusDexterity = statRoll;
                armor.itemName = $"Normal {armor.armorType} {armor.slot}";
                armor.requiredLevel = 3;
            }
            else if (statRoll > 10 && statRoll <= 15 && armor.armorType == ArmorTypes.LEATHER)
            {
                armor.bonusDexterity = statRoll;
                armor.itemName = $"Greater {armor.armorType} {armor.slot}";
                armor.requiredLevel = 5;
            }
            else if (statRoll > 15 && statRoll <= 20 && armor.armorType == ArmorTypes.LEATHER)
            {
                armor.bonusDexterity = statRoll;
                armor.itemName = $"Vidala's {armor.armorType} {armor.slot}";
                armor.requiredLevel = 7;
            }

            // Stat roll for mail gear
            else if (statRoll <= 5 && armor.armorType == ArmorTypes.MAIL)
            {
                if (statRoll % 2 != 0) { statRoll += 1; }
                armor.bonusDexterity = statRoll / 2;
                armor.bonusStrength = statRoll / 2;
                armor.itemName = $"Poor {armor.armorType} {armor.slot}";
                armor.requiredLevel = 1;
            }
            else if (statRoll > 5 && statRoll <= 10 && armor.armorType == ArmorTypes.MAIL)
            {
                if (statRoll % 2 != 0) { statRoll += 1; }
                armor.bonusDexterity = statRoll / 2;
                armor.bonusStrength = statRoll / 2;
                armor.itemName = $"Normal {armor.armorType} {armor.slot}";
                armor.requiredLevel = 3;
            }
            else if (statRoll > 10 && statRoll <= 15 && armor.armorType == ArmorTypes.MAIL)
            {
                if (statRoll % 2 != 0) { statRoll += 1; }
                armor.bonusDexterity = statRoll / 2;
                armor.bonusStrength = statRoll / 2;
                armor.itemName = $"Greater {armor.armorType} {armor.slot}";
                armor.requiredLevel = 5;
            }
            else if (statRoll > 15 && statRoll <= 20 && armor.armorType == ArmorTypes.MAIL)
            {
                if (statRoll % 2 != 0) { statRoll += 1; }
                armor.bonusDexterity = statRoll / 2;
                armor.bonusStrength = statRoll / 2;
                armor.itemName = $"Milabrega's {armor.armorType} {armor.slot}";
                armor.requiredLevel = 7;
            }

            //stat roll for plate gear
            else if (statRoll <= 5 && armor.armorType == ArmorTypes.PLATE)
            {
                armor.bonusStrength = statRoll;
                armor.itemName = $"Poor {armor.armorType} {armor.slot}";
                armor.requiredLevel = 1;
            }
            else if (statRoll > 5 && statRoll <= 10 && armor.armorType == ArmorTypes.PLATE)
            {
                armor.bonusStrength = statRoll;
                armor.itemName = $"Normal {armor.armorType} {armor.slot}";
                armor.requiredLevel = 3;
            }
            else if (statRoll > 10 && statRoll <= 15 && armor.armorType == ArmorTypes.PLATE)
            {
                armor.bonusStrength = statRoll;
                armor.itemName = $"Greater {armor.armorType} {armor.slot}";
                armor.requiredLevel = 5;
            }
            else if (statRoll > 15 && statRoll <= 20 && armor.armorType == ArmorTypes.PLATE)
            {
                armor.bonusStrength = statRoll;
                armor.itemName = $"Immortal king's {armor.armorType} {armor.slot}";
                armor.requiredLevel = 7;
            }
            return armor;
        }

    }
}
