﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AssignmentOne.Items
{
    /// <summary>
    /// generic itemclass with properties all types of item inherit.
    /// </summary>
    public abstract class GenericItem
    {
        public string itemName { set; get; }
        public int requiredLevel { set; get; }
        public Slots slot { set; get; }
    }
}
