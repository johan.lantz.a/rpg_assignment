﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AssignmentOne.Character;

namespace AssignmentOne.Items
{
    public class Monster
    {

        public double monsterHealth { get; set; }
        public double monsterDamage { get; set; }
        public double expYield { get; set; }
        public string monsterName { get; set; }

        public double monsterLevel { get; set; }

        public bool monsterDefence { get; set; }    
        /// <summary>
        /// The generateMonster function creates a monster with semirandomized stats depending on the player.level. 
        /// </summary>
        /// <param name="player"></param>
        public static Monster generateMonster(PlayerClass player)
        {

            Monster monster = new Monster();
            Random random = new Random();  


            monster.monsterLevel = random.Next(player.level - 1, player.level + 3);

            if (monster.monsterLevel < 1)
            {
                monster.monsterLevel = 1;
            }

            monster.monsterDamage = monster.monsterLevel * 2;


            monster.monsterHealth = (monster.monsterLevel * 4);

            if (monster.monsterLevel > player.level)
            {
                double expModifier = monster.monsterLevel - player.level;
                monster.expYield = (1 + expModifier * 0.2) * 10;
            }
            else if (monster.monsterLevel == player.level)
            {
                monster.expYield = 10;
            }
            else
            {
                monster.expYield = 8;
            }

            if (monster.monsterLevel < 4)
            {
                monster.monsterName = "Fallen one";
            }
            else if (monster.monsterLevel >= 4 && monster.monsterLevel < 8)
            {
                monster.monsterName = "Megademon";
            }
            else if (monster.monsterLevel >= 8 && monster.monsterLevel < 10)
            {
                monster.monsterName = "Overseer";
            }
            else
            {
                monster.monsterName = "Cow King";
            }

            return monster;
        }


        /// <summary>
        /// The monsterAction function uses random.Next to determine the monster action between a normal attack (outcome 1 and 2), a critical hit (outcome 3)
        /// or changing monsterDefence boolean to true (outcome 4). The function takes in a previously generated monster to determine the monster attack which i returns
        /// and a boolean of counter too determine if the damage is reduced by player action (countering or not).
        /// </summary>
        /// <param name="monster"></param>
        /// <param name="counter"></param>
        /// <returns> Return the dps / damage that the monster will inflict on its turn depending on monsterstat and if the player countered or not </returns>
        public double monsterAction(Monster monster, bool counter)
        {

            double tempMonsterDPS = monster.monsterDamage;
           
            monsterDefence = false;

            Random random = new Random();
            int monsterAction = random.Next(1,5);

            if (monsterAction == 1 || monsterAction == 2) //attacks player normaly
            {
                if (counter == true)
                {
                    tempMonsterDPS = tempMonsterDPS * 0.7;
                    
                }

                else
                {
                    tempMonsterDPS = tempMonsterDPS;
                }

            }else if (monsterAction == 3) //attacks player with a crit
            {
                if (counter == true)
                {
                    tempMonsterDPS = tempMonsterDPS;
                    
                }
                else
                {
                    tempMonsterDPS = tempMonsterDPS * 1.5;
                }              

            }else if (monsterAction == 4) //defends against incoming attack
            {
                
                monsterDefence = true;
            }

            return tempMonsterDPS;
        }
    }
}
