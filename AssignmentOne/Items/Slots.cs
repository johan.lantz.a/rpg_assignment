﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AssignmentOne.Items
{
    /// <summary>
    /// all possible equipment slots
    /// </summary>
    public enum Slots
    {
        HEAD,
        BODY,
        LEGS,
        WEAPON
    }
}
