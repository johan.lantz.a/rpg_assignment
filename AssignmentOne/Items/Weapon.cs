﻿using AssignmentOne.Character;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AssignmentOne.Items
{
    public class Weapon : GenericItem
    {
        public Weapon()
        {
            
        }
        public double attackSpeed { get; set; }   
        public double damage { get; set; }
        public double DPS { get => attackSpeed * damage;} 
        public WeaponTypes weapontype { get; set; }


        /// <summary>
        /// function that uses random to gain a value between 1-7 to determine the weapontype and 1-10 determine the stats on the weapon
        /// </summary>
        /// <returns>A randomly rolled weapon</returns>
        public static Weapon CreateWeapon()
        {

            Weapon weapon = new Weapon();
            Random random = new Random();

            // Roll for weapontype on newly created weapon. Later used in if / else if to assign weapontype
            int wepType = random.Next(1, 8);


            // Roll for stats which determines the power, name, and level requirement for weapon
            int stat = random.Next(1, 11);
           
            if (stat == 10)
            {
                int ancient = random.Next(1,11);
                if (ancient == 10)
                {
                    stat = 11;
                }
            }

           
            // Assigning weapontype based on randomized type roll
            if (wepType == 1)
            {
                weapon.weapontype = WeaponTypes.AXE;
            }
            else if (wepType == 2)
            {
                weapon.weapontype = WeaponTypes.BOW;
            }
            else if (wepType == 3)
            {
                weapon.weapontype = WeaponTypes.DAGGER;
            }
            else if (wepType == 4)
            {
                weapon.weapontype = WeaponTypes.HAMMER;
            }
            else if (wepType == 5)
            {
                weapon.weapontype = WeaponTypes.STAFF;
            }
            else if (wepType == 6)
            {
                weapon.weapontype = WeaponTypes.SWORD;
            }
            else if (wepType == 7)
            {
                weapon.weapontype = WeaponTypes.WAND;
            }

          
            // Assigning stats to weapon based on randomized stat roll
            if (stat <= 3) 
            {
                weapon.itemName = $"Poor {weapon.weapontype}";
                weapon.attackSpeed = 1;
                weapon.damage = stat;
                weapon.requiredLevel = stat;
            }
            else if (stat >= 4 && stat <= 6)
            {
                weapon.itemName = $"Normal {weapon.weapontype}";
                weapon.attackSpeed = 2;
                weapon.damage = stat;
                weapon.requiredLevel = stat -1;
            }
            else if (stat >= 7 && stat <= 9)
            {
                weapon.itemName = $"Greater {weapon.weapontype}";
                weapon.attackSpeed = 3;
                weapon.damage = stat;
                weapon.requiredLevel = stat -1;
            }
            else if (stat == 10)
            {
                weapon.itemName = $"Legendary {weapon.weapontype}";
                weapon.attackSpeed = 4;
                weapon.damage = stat;
                weapon.requiredLevel = stat -1;
            }
            else if (stat == 11)
            {
                weapon.itemName = $"Ancient Legendary {weapon.weapontype}";
                weapon.attackSpeed = 6;
                weapon.damage = stat;
                weapon.requiredLevel = stat-2;
            }

            
            // Returning finished weapon
            return weapon;
        }


        /// <summary>
        /// Similar to earlier created CreateWeapon function, but this one is called upon killing a monster in fightMonster function (inside of playerClass file). 
        /// Randomized in same way the other CreateWeapon function does, but limits the item too one that the player object can equip.
        /// </summary>
        /// <param name="player"></param>
        /// <returns> Returns a weapon with limited stats and type to one that the player item can equip </returns>
        public static Weapon CreateWeaponLoot(PlayerClass player)
        {

            Weapon weapon = new Weapon();
            Random random = new Random();


            int wepType = 0;

            //Rolls for weapontypes depending on which weapontypes the player object can equip.

            if (player.EquipableWeapons.Contains(WeaponTypes.AXE) && player.EquipableWeapons.Contains(WeaponTypes.SWORD) && player.EquipableWeapons.Contains(WeaponTypes.HAMMER))
            {
                wepType = random.Next(1, 4);
            }
            else if (player.EquipableWeapons.Contains(WeaponTypes.WAND) && player.EquipableWeapons.Contains(WeaponTypes.STAFF))
            {
                wepType = random.Next(5,7);
            }
            else if (player.EquipableWeapons.Contains(WeaponTypes.BOW))
            {
                wepType = 7;
            }
            else if (player.EquipableWeapons.Contains(WeaponTypes.SWORD) && player.EquipableWeapons.Contains(WeaponTypes.DAGGER)) 
            {
                wepType = random.Next(3, 5);
            }
           


            // Randomized stat roll which will determine the weapon power, name, and leverequirement. 

            int stat = random.Next(1, 11);

            if (stat == 10)
            {
                int ancient = random.Next(1, 11);
                if (ancient == 10)
                {
                    stat = 11;
                }
            }

            // Assigning weapontype to the new Weapon object.

            if (wepType == 1)
            {
                weapon.weapontype = WeaponTypes.AXE;
            }
            else if (wepType == 2)
            {
                weapon.weapontype = WeaponTypes.HAMMER;
                
            }
            else if (wepType == 3)
            {
                weapon.weapontype = WeaponTypes.SWORD;
                
            }
            else if (wepType == 4)
            {
                weapon.weapontype = WeaponTypes.DAGGER;
                
            }
            else if (wepType == 5)
            {
                weapon.weapontype = WeaponTypes.STAFF;
            }
            else if (wepType == 6)
            {
                weapon.weapontype = WeaponTypes.WAND;
            }
            else if (wepType == 7)
            {
                weapon.weapontype = WeaponTypes.BOW;        
            }


            // If / else if statements to assure the weapon that is generated does not have a higher levelrequirement than the player.level

            if (player.level <= 3 && stat > player.level)
            {
                stat = player.level;
            }
            else if (player.level > 3 && player.level < 10 && stat > player.level && stat < 11)
            {
                stat = player.level + 1;
            }
            else if (player.level > 8 && stat > 9)
            {
                stat = player.level + 1;
            }
         
            // Assigning stats to the weapon depending on stat value.

            if (stat <= 3)
            {
                weapon.itemName = $"Poor {weapon.weapontype}";
                weapon.attackSpeed = 1;
                weapon.damage = stat;
                weapon.requiredLevel = stat;
            }
            else if (stat >= 4 && stat <= 6)
            {
                weapon.itemName = $"Normal {weapon.weapontype}";
                weapon.attackSpeed = 2;
                weapon.damage = stat;
                weapon.requiredLevel = stat - 1;
            }
            else if (stat >= 7 && stat <= 9)
            {
                weapon.itemName = $"Greater {weapon.weapontype}";
                weapon.attackSpeed = 3;
                weapon.damage = stat;
                weapon.requiredLevel = stat - 1;
            }
            else if (stat == 10)
            {
                weapon.itemName = $"Legendary {weapon.weapontype}";
                weapon.attackSpeed = 4;
                weapon.damage = stat;
                weapon.requiredLevel = stat - 1;
            }
            else if (stat == 11)
            {
                weapon.itemName = $"Ancient Legendary {weapon.weapontype}";
                weapon.attackSpeed = 6;
                weapon.damage = stat;
                weapon.requiredLevel = stat - 2;
            }


            //Returns the finished weapon.
            return weapon;
        }

    }
    
}
