﻿
using AssignmentOne.Character;
using AssignmentOne.Items;

Console.WriteLine("Enter name for your character:");
string PlayerName = Console.ReadLine();

/// <summary>
/// creation of starter weapon for all pickable classes
/// </summary>
Weapon weapon = new Weapon();
Weapon rangerWeapon = new Weapon() { attackSpeed = 1, damage = 1, itemName = "Cracked BOW", requiredLevel = 1, slot = Slots.WEAPON, weapontype = WeaponTypes.BOW};
Weapon warriorWeapon = new Weapon() { attackSpeed = 1, damage = 1, itemName = "Cracked SWORD", requiredLevel = 1, slot = Slots.WEAPON, weapontype = WeaponTypes.SWORD};
Weapon MageWeapon = new Weapon() { attackSpeed = 1, damage = 1, itemName = "Cracked STAFF", requiredLevel = 1, slot = Slots.WEAPON, weapontype = WeaponTypes.STAFF };
Weapon rogueWeapon = new Weapon() { attackSpeed = 1, damage = 1, itemName = $"Cracked DAGGER", requiredLevel = 1, slot = Slots.WEAPON, weapontype = WeaponTypes.DAGGER };


PlayerClass playerChar = new();
bool classBool = true;

// if / else if statement for player to pick class.
do
{
    do
    {  
    Console.WriteLine("Enter 1 to play a warrior, 2 to play a rogue, 3 to play a ranger, 4 to play a mage");
    string ClassPick = Console.ReadLine();

    

    if (ClassPick == "1")
    {
        Warrior WarriorCharacter = new();
        WarriorCharacter.Name = PlayerName;
        Console.WriteLine($" You are now a Warrior!");
        playerChar = WarriorCharacter;
        weapon = warriorWeapon;
        playerChar.equipment[Slots.WEAPON] = weapon;
            classBool = false;

    }
    else if (ClassPick == "2")
    {
        Rogue RogueCharacter = new();
        RogueCharacter.Name = PlayerName;
        Console.WriteLine($" You are now a Rogue!");
        playerChar = RogueCharacter;
        weapon = rogueWeapon;
        playerChar.equipment[Slots.WEAPON] = weapon;
            classBool = false;
    }
    else if (ClassPick == "3")
    {
        Ranger RangerCharacter = new();
        RangerCharacter.Name = PlayerName;
        Console.WriteLine($" You are now a Ranger!");
        playerChar = RangerCharacter;
        weapon = rangerWeapon;
        playerChar.equipment[Slots.WEAPON] = weapon;
            classBool = false;
    }
    else if (ClassPick == "4")
    {
        Mage MageCharacter = new();


        MageCharacter.Name = PlayerName;
        Console.WriteLine($" You are now a Mage!");
        playerChar = MageCharacter;
        weapon = MageWeapon;
        playerChar.equipment[Slots.WEAPON] = weapon;
            classBool = false;
    }
    else
    {
        Console.WriteLine("incorrect input");
    }
    } while (classBool == true);


    // player input to level up, display stats or change equipment (includes generating random loot and equipping).

    do
    {
        playerChar.calcDamage();
        Console.WriteLine("1 to attack a monster ::: Press 2 to display your stats ::: Press 3 to change equipment (3 is cheat for easy gearing)");
        string PlayerInput = Console.ReadLine();

        if (PlayerInput == "1")
        {
            //Generates monster with stats and level depending on the player object level
            Monster monster = Monster.generateMonster(playerChar);

            //Starts the "fight" between player object and monster. (code located in the PlayerClass file).
            playerChar.fightMonster(playerChar, monster);
            
        }
        else if (PlayerInput == "2") // Prints out different stats related to the player object. The primary damage stat is dependant on the playerclass
                                     // Example: Strength for warrior and Intelligence for Mage.
        {
            Console.WriteLine(PlayerClass.showStats(playerChar));
            Weapon printWeapon = (Weapon)playerChar.equipment[Slots.WEAPON];
            Console.WriteLine($"Your wep is a: {printWeapon.itemName}");
            Console.WriteLine($"Your character's dps is: {playerChar.characterDPS} and you have {playerChar.vitality} health");
            Console.WriteLine($"Your primary damage stat is: {playerChar.primaryDamageStat}");
            Console.WriteLine($"You have {playerChar.experience} experience and need {10 - playerChar.experience} too level up!");
            Console.WriteLine("Press enter to go back to actions menu");
        }
        else if (PlayerInput == "3") // Calls upon the CreateWeapon or CreateArmor functions to create new randomized items
        {
            Console.WriteLine($"Select slot to modify equipment in. Press 1 for: {Slots.WEAPON}. " +
                $"Press 2 for: armor item");
            string slotModify = Console.ReadLine();
            if (slotModify == "1")
            {
                var newWeapon = Weapon.CreateWeapon();      
                    weapon = (Weapon)playerChar.equipment[Slots.WEAPON];
                    Console.WriteLine($"A {newWeapon.itemName} with required level {newWeapon.requiredLevel} dropped!");
                    Console.WriteLine($"Old weapon dps: {weapon.DPS}. New weapon dps: {newWeapon.DPS}");
                    Console.WriteLine("Do you want to equip new weapon? Enter 1 to equip new weapon and 2 to discard new weapon");
                    string eqWep = Console.ReadLine();
                    if (eqWep == "1")
                    {
                        try
                        {
                            playerChar.equipWep(playerChar, newWeapon);
                            
                        }
                        catch (Exception e)
                        {
                            Console.WriteLine(e.Message);
                        }
                }  
            }
            else if (slotModify == "2")
            {
            var newArmor = Armor.CreateArmor();
                Console.WriteLine($"A {newArmor.itemName} with required level {newArmor.requiredLevel} dropped!");
                Console.WriteLine(" Do you want to equip the new armor piece? Enter 1 to equip and 2 to discard the new item");
                string eqArmor = Console.ReadLine();
                if (eqArmor == "1")
                {
                    try
                    {
                        playerChar.equipArmor(playerChar, newArmor);
                    }
                    catch (Exception e)
                    {
                        Console.WriteLine(e.Message);
                    }
                }
            }
        }
    } while (Console.ReadLine() != "q"); // Both q inputs are temp inputs to close cancel do while loop and closing the console application.
} while (Console.ReadLine() != "q");

