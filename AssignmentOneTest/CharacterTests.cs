using AssignmentOne.Character;

namespace AssignmentOneTest
{
    public class CharacterTests
    {
        [Fact]
        public void NewCharacter_CreatingNewCharacter_ShouldBeLevelOne()
        {
            //Arrange
            PlayerClass testCharacter = new PlayerClass();
            int expected = 1;

            //Act
            int actual = testCharacter.level;

            //Assert
            Assert.Equal(expected, actual);
        }
        [Fact]
        public void LevelUp_LevelingUpCharacter_ShouldBeLevelTwo()
        {
            //Arrange
            PlayerClass testCharacter = new PlayerClass();
            int expected = 2;
            
            //Act
            testCharacter.levelUp();
            int actual = testCharacter.level;

            //Assert
            Assert.Equal(expected,actual);

        }
        [Fact]
        public void WarriorAttributes_NewCharacterAttributes_ShouldBeLevelOneAttributes()
        {
            //Arrange
            Warrior testWarrior = new Warrior();
            int expectedStrength = 5;
            int expectedDexterity = 2;
            int expectedIntelligence = 1;
            int[] expected = new int[] {expectedStrength, expectedDexterity,expectedIntelligence};

            //Act
            int actualStrength = testWarrior.strength;
            int actualDexterity = testWarrior.dexterity;
            int actualIntelligence = testWarrior.intelligence;
            int[] actual = new int[] {actualStrength,actualDexterity,actualIntelligence};

            //Assert
            Assert.Equal(expected, actual);
        }

        //Tests for new character stats

        [Fact]
        public void MageAttributes_NewCharacterAttributes_ShouldBeLevelOneAttributes()
        {
            //Arrange
            Mage testMage = new Mage();
            int expectedStrength = 1;
            int expectedDexterity = 1;
            int expectedIntelligence = 8;
            int[] expected = new int[] { expectedStrength, expectedDexterity, expectedIntelligence };

            //Act
            int actualStrength = testMage.strength;
            int actualDexterity = testMage.dexterity;
            int actualIntelligence = testMage.intelligence;
            int[] actual = new int[] { actualStrength, actualDexterity, actualIntelligence };

            //Assert
            Assert.Equal(expected, actual);
        }
        [Fact]
        public void RogueAttributes_NewCharacterAttributes_ShouldBeLevelOneAttributes()
        {
            //Arrange
            Rogue testRogue = new Rogue();
            int expectedStrength = 2;
            int expectedDexterity = 6;
            int expectedIntelligence = 1;
            int[] expected = new int[] { expectedStrength, expectedDexterity, expectedIntelligence };

            //Act
            int actualStrength = testRogue.strength;
            int actualDexterity = testRogue.dexterity;
            int actualIntelligence = testRogue.intelligence;
            int[] actual = new int[] { actualStrength, actualDexterity, actualIntelligence };

            //Assert
            Assert.Equal(expected, actual);
        }
        [Fact]
        public void RangerAttributes_NewCharacterAttributes_ShouldBeLevelOneAttributes()
        {
            //Arrange
            Ranger testRanger = new Ranger();
            int expectedStrength = 1;
            int expectedDexterity = 7;
            int expectedIntelligence = 1;
            int[] expected = new int[] { expectedStrength, expectedDexterity, expectedIntelligence };

            //Act
            int actualStrength = testRanger.strength;
            int actualDexterity = testRanger.dexterity;
            int actualIntelligence = testRanger.intelligence;
            int[] actual = new int[] { actualStrength, actualDexterity, actualIntelligence };

            //Assert
            Assert.Equal(expected, actual);
        }




        //Tests for leveling up a character and checking stats

        [Fact]
        public void WarriorLevelUppAttributes_LevelTwoCharacterAttributes_ShouldBeLevelTwoAttributes()
        {
            //Arrange
            Warrior testWarrior = new Warrior();
            int expectedStrength = 8;
            int expectedDexterity = 4;
            int expectedIntelligence = 2;
            int[] expected = new int[] { expectedStrength, expectedDexterity, expectedIntelligence };

            //Act
            testWarrior.levelUp();
            int actualStrength = testWarrior.strength;
            int actualDexterity = testWarrior.dexterity;
            int actualIntelligence = testWarrior.intelligence;
            int[] actual = new int[] { actualStrength, actualDexterity, actualIntelligence };

            //Assert
            Assert.Equal(expected, actual);
        }
        [Fact]
        public void MageLevelUppAttributes_LevelTwoCharacterAttributes_ShouldBeLevelTwoAttributes()
        {
            //Arrange
            Mage testMage = new Mage();
            int expectedStrength = 2;
            int expectedDexterity = 2;
            int expectedIntelligence = 13;
            int[] expected = new int[] { expectedStrength, expectedDexterity, expectedIntelligence };

            //Act
            testMage.levelUp();
            int actualStrength = testMage.strength;
            int actualDexterity = testMage.dexterity;
            int actualIntelligence = testMage.intelligence;
            int[] actual = new int[] { actualStrength, actualDexterity, actualIntelligence };

            //Assert
            Assert.Equal(expected, actual);
        }
        [Fact]
        public void RogueLevelUppAttributes_LevelTwoCharacterAttributes_ShouldBeLevelTwoAttributes()
        {
            //Arrange
            Rogue testRogue = new Rogue();
            int expectedStrength = 3;
            int expectedDexterity = 10;
            int expectedIntelligence = 2;
            int[] expected = new int[] { expectedStrength, expectedDexterity, expectedIntelligence };

            //Act
            testRogue.levelUp();
            int actualStrength = testRogue.strength;
            int actualDexterity = testRogue.dexterity;
            int actualIntelligence = testRogue.intelligence;
            int[] actual = new int[] { actualStrength, actualDexterity, actualIntelligence };

            //Assert
            Assert.Equal(expected, actual);
        }
        [Fact]
        public void RangerLevelUppAttributes_LevelTwoCharacterAttributes_ShouldBeLevelTwoAttributes()
        {
            //Arrange
            Ranger testRanger = new Ranger();
            int expectedStrength = 2;
            int expectedDexterity = 12;
            int expectedIntelligence = 2;
            int[] expected = new int[] { expectedStrength, expectedDexterity, expectedIntelligence };

            //Act
            testRanger.levelUp();
            int actualStrength = testRanger.strength;
            int actualDexterity = testRanger.dexterity;
            int actualIntelligence = testRanger.intelligence;
            int[] actual = new int[] { actualStrength, actualDexterity, actualIntelligence };

            //Assert
            Assert.Equal(expected, actual);
        }
    }
}