﻿using AssignmentOne.Character;
using AssignmentOne.CustomExceptions;
using AssignmentOne.Items;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AssignmentOneTest
{
    public class EquipmentTests
    {
        [Fact]
        public void equipWep_EquipWeaponWithTooHighLevelReq_ShouldThrowError()
        {
            //Arrange
            Warrior testWarrior = new Warrior();

            Weapon testWeapon = new Weapon()
            {
                weapontype = WeaponTypes.AXE,
                itemName = $"poor axe",
                slot = Slots.WEAPON,
                requiredLevel = 2,
                attackSpeed = 1,
                damage = 1,

            };

            //Act

            //Assert
            Assert.Throws<WeaponException>(() => testWarrior.equipWep(testWarrior, testWeapon));

        }
        [Fact]
        public void equipArmot_EquipArmorWithTooHighLevelReq_ShouldThrowError()
        {

            //Arrange
            Warrior testWarrior = new Warrior();

            Armor testArmor = new Armor()
            {
                
                armorType = ArmorTypes.PLATE,
                itemName = "poor body armor",
                requiredLevel = 2,
                slot = Slots.BODY,      
            };

            //Act
            //Assert
            Assert.Throws<ArmorException>(() => testWarrior.equipArmor(testWarrior, testArmor));
        }
        [Fact]
        public void equipWep_EquipWeaponOutsideOfAllowedWeapontypesForCharacter_ShouldThrowError()
        {
            //Arrange
            var testWarrior = new Warrior();

            Weapon testWeapon = new Weapon()
            {
                weapontype = WeaponTypes.BOW,
                itemName = $"poor bow",
                slot = Slots.WEAPON,
                requiredLevel = 1,
                attackSpeed = 1,
                damage = 1,
            };
            

            //act
            //Assert
            Assert.Throws<WeaponException>(() => testWarrior.equipWep(testWarrior, testWeapon));
        }

        [Fact]
        public void equipArmor_EquipArmornOutsideOfAllowedWeapontypesForCharacter_Should_throw_error()
        {
            //Arrange
            Warrior testWarrior = new Warrior();

            Armor testArmor = new Armor()
            {

                armorType = ArmorTypes.CLOTH,
                itemName = "poor body armor",
                requiredLevel = 1,
                slot = Slots.BODY,
                
            };

            //Act
            //Assert
            Assert.Throws<ArmorException>(()=> testWarrior.equipArmor(testWarrior, testArmor));
        }

        [Fact]
        public void equipWep_EquipNewWeapon_ReturnConfirmationString()
        {
            //Arrange
            Warrior testWarrior = new Warrior();

            Weapon testWeapon = new Weapon()
            {
                weapontype = WeaponTypes.SWORD,
                itemName = $"poor Sword",
                slot = Slots.WEAPON,
                requiredLevel = 1,
                attackSpeed = 1,
                damage = 1,

            };

            //Act
            string actual = testWarrior.equipWep(testWarrior,testWeapon);
            string expected = "New weapon equipped";

            //Assert
            Assert.Equal(expected,actual);  

        }

        [Fact]
        public void equipArmor_EquipNewArmor_ReturnConfirmationString()
        {
            //Arrange
            Warrior testWarrior = new Warrior();

            Armor testArmor = new Armor()
            {
                armorType = ArmorTypes.PLATE,
                itemName = "poor body armor",
                requiredLevel = 1,
                slot = Slots.BODY,
            };

            //Act
            string actual = testWarrior.equipArmor(testWarrior, testArmor);
            string expected = "New armor equipped!";

            //Assert
            Assert.Equal(expected ,actual);

        }

        [Fact]
        public void calcDamage_CalculateDamageOfWarriorWithNoWeapon_ShouldReturnExpectedDamage()
        {
            //Arrange
            Warrior testWarrior = new Warrior();

            //Act
            testWarrior.calcDamage();
            double expected = 1.0 * (1.0 + (5.0 / 100.0));

            //Assert
            Assert.Equal(expected,testWarrior.characterDPS);

        }

        [Fact]
        public void calcDamage_CalculateDamageOfWarriorWithWeapon_ShouldReturnExpectedDamage()
        {
            //Arrange
            Warrior testWarrior = new Warrior();

            Weapon testWeapon = new Weapon() {
                weapontype = WeaponTypes.AXE,
                attackSpeed = 1.1,
                damage = 7,
                requiredLevel = 1,
                slot = Slots.WEAPON,
                itemName = "Meh axe",
            };

            //Act
            testWarrior.equipWep(testWarrior,testWeapon);
            double expected = (7.0 * 1.1) * (1.0 + (5.0 / 100.0));

            //Assert
            Assert.Equal(expected, testWarrior.characterDPS);

        }

        [Fact]
        public void calcDamage_CalculateDamageOfWarriorWithWeaponAndArmor_ShouldReturnExpectedDamage()
        {
            //Arrange
            Warrior testWarrior = new Warrior();

            Weapon testWeapon = new Weapon()
            {
                weapontype = WeaponTypes.AXE,
                attackSpeed = 1.1,
                damage = 7,
                requiredLevel = 1,
                slot = Slots.WEAPON,
                itemName = "Meh axe",
            };
            Armor testArmor = new Armor() { 
                armorType = ArmorTypes.PLATE,
                slot = Slots.BODY,
                requiredLevel = 1,
                bonusStrength = 1,
                itemName = "Meh body armor",
            };

            //Act
            testWarrior.equipArmor(testWarrior, testArmor);
            testWarrior.equipWep(testWarrior, testWeapon);
            double expected = (7.0 * 1.1) * (1.0 + ((5.0 + 1) / 100.0));

            //Assert
            Assert.Equal(expected, testWarrior.characterDPS);

        }
    }
}
