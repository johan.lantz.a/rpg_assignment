# RPG Project


## Description
RPG project is a simple console application in visual studios using c#. You can create your own character and pick between 4 different classes. 
To modify your character you can level it up and change the equipment it has to enhance its powers. If you run this code in the console all the loot (equipment) is generated using random variables and you wont be able too use all dropped items. 

## Usage
When starting the console application you are asked to input a charactername and then pick between 4 different classes: Mage, Warrior, Ranger and Rogue. 

Once in the application you can make use of 3 different functions:

Fight monster starts an encounter with a randomized monster that is set to be between 1 level lower and up too 3 levels above the player character. Do be careful since the monsters are touch and will defeat you incase you make a misstake or stay too long. In the battle you can either attack, counter or flee. Attack will use your character dps and damage the monsters health depending on the monsters own action. Counter will reduce the incoming damage from the monster, and even return some of the inflicted damage back to the monster. If the monster crits 150 % of the damage is returned! Lastly the flee option lets you return to the base menu of fightning monster, showing stats or obtaining free loot (cheating).

If you defeat the monster you will be rewarded with either a weapon or armor piece that is made to be equipable for your character! But if you die, the entire application will shut down and you will have to start over.

Displaying stats allows you to view information related to your character (such as level, attributes, damage and health).

## Tests
There are multiple tests available to see and test in the AssignmentONeTest folder. 
Some example of tests for character and items are:

```NewCharacter_CreatingNewCharacter_ShouldBeLevelOne()```
Checks the level for a newly created character object.

```LevelUp_LevelingUpCharacter_ShouldBeLevelTwo()```
Checks the level of a newly created character object that has called the levelUp function.

```WarriorLevelUppAttributes_LevelTwoCharacterAttributes_ShouldBeLevelTwoAttributes()```
Checks the values of attribues of a warrior object that has used the levelUp function.

```equipWep_EquipWeaponWithTooHighLevelReq_ShouldThrowError()```
Checks if equipWep function throws error when used with weapon that has too high level requirement.

```equipWep_EquipWeaponOutsideOfAllowedWeapontypesForCharacter_ShouldThrowError()```
Checks if equipWep function throws error when used with incompatible weapon and character class-

```calcDamage_CalculateDamageOfWarriorWithWeaponAndArmor_ShouldReturnExpectedDamage()```
Uses the calcDamage function and checks if the correct calculations for characterdamage is done with specified weapon and weaponstats.



## Authors
Johan Lantz([johan.lantz.a](https://gitlab.com/johan.lantz.a))

## Contributing
Pull requests are welcome. For major changes, please open an issue to discuss what you would change.
